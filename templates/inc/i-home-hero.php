<div class="hero">
	<div class="swiper-wrapper button-controls">
		<div class="swipe" data-auto="7" data-controls="true">
			<div class="swipe-wrap">

				<div data-src="http://www.energyco.ca.php54-2.dfw1-2.websitetestlink.com/wp-content/uploads/2014/12/energy-co.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="hero-content">
						<span class="hero-title">Your Energy Starts Here</span>
						<span class="hero-subtitle">The Energy Company is a fitness &amp; health company based in St. John's, NL</span>
						<a href="#" class="button big">Find Out More</a>						
					</div><!-- .hero-content -->
					
				</div>

				<div data-src="http://www.fillmurray.com/1920/1100">
					<div class="item">&nbsp;</div>
					
					<div class="hero-content">
						<span class="hero-title">Your Energy Starts Here</span>
						<span class="hero-subtitle">The Energy Company is a fitness &amp; health company based in St. John's, NL</span>
						<a href="#" class="button big">Find Out More</a>						
					</div><!-- .hero-content -->
					
				</div>				

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->