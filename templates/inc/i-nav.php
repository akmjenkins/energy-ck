		<div class="nav">
		
			<button class="toggle-nav">
				<span>&nbsp;</span> Menu
			</button>
			
			<nav>
				<ul>
					<li><a href="#">Strength</a></li>
					<li><a href="#">Corporate</a></li>
					<li><a href="#">Safety</a></li>
					<li><a href="#">About Us</a></li>
					<li><a href="#">The Latest</a></li>
				</ul>
			</nav>
		
			<div class="nav-top">
				<a href="#">Login</a>
				<a href="#">FAQ</a>
				<a href="#">Contact</a>
				<a class="ts-wrap">
					<span class="toggle-search fa-search t-fa-abs">Search</span>
				</a>
			</div><!-- .nav-top -->

			<form action="/" method="get" class="single-form search-form">
				<fieldset>
					<input type="text" name="s" placeholder="Search">
					<button type="submit" class="fa-search t-fa-abs">Search</button>
				</fieldset>
			</form>

		</div><!-- .nav -->