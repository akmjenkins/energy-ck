<div class="grid eqh always-list blocks">

	<div class="col-3 col">
		<a class="item" href="#">
		
			<div class="block with-button">
				<div class="img-wrap">
					<div class="img lazybg immediate" data-src="../assets/images/temp/latest/1.jpg"></div>
				</div><!-- .img-wrap -->
				<div class="content">
				
					<div class="time">
						
						<time datetime="2014-06-15">
							<span class="day">15</span>
							
							<span class="month-year">
								<span class="month">Jun</span>
								<span class="year">2014</span>
							</span><!-- .month-year -->
							
						</time>
						
					</div><!-- .time -->
				
					<div class="title-block">
						<span class="h4-style heading title">This is a news</span>
					</div>
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
					
					<span class="button">Read More</span>
					
				</div><!-- .content -->
			</div><!-- .block -->
			
		</a><!-- .item -->
	</div><!-- .col -->

	<div class="col-3 col">
		<a class="item" href="#">
		
			<div class="block with-button">
				<div class="img-wrap">
					<div class="img lazybg immediate" data-src="../assets/images/temp/latest/1.jpg"></div>
				</div><!-- .img-wrap -->
				<div class="content">
				
					<div class="time">
						
						<time datetime="2014-06-15">
							<span class="day">15</span>
							
							<span class="month-year">
								<span class="month">Jun</span>
								<span class="year">2014</span>
							</span><!-- .month-year -->
							
						</time>
						
					</div><!-- .time -->
				
					<div class="title-block">
						<span class="h4-style heading title">This is a news</span>
						<span class="h5-style heading subtitle">Sub Title</span>
					</div>
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
					
					<span class="button">Read More</span>
					
				</div><!-- .content -->
			</div><!-- .block -->
			
		</a><!-- .item -->
	</div><!-- .col -->

	<div class="col-3 col">
		<a class="item bounce" href="#">
		
			<div class="block with-button">
				<div class="img-wrap">
					<div class="img lazybg immediate" data-src="../assets/images/temp/latest/1.jpg"></div>
				</div><!-- .img-wrap -->
				<div class="content">
				
					<div class="time">
						
						<time datetime="2014-06-15">
							<span class="day">15</span>
							
							<span class="month-year">
								<span class="month">Jun</span>
								<span class="year">2014</span>
							</span><!-- .month-year -->
							
						</time>
						
					</div><!-- .time -->
				
					<div class="title-block">
						<span class="h4-style heading title">This is a news</span>
						<span class="h5-style heading subtitle">Sub Title</span>
					</div>
					
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. 
					Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor.</p>
					
					<span class="button">Read More</span>
					
				</div><!-- .content -->
			</div><!-- .block -->
			
		</a><!-- .item -->
	</div><!-- .col -->

	
</div><!-- .grid -->