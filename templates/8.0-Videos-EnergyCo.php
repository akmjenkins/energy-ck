<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">

			
				<article>
				
					<section class="dark-bg bluegrad">
						<div class="sw">
							
							<div class="article-flex">
								<div class="article-title">
									<span class="circle-button big blue fa-video-camera">Video</span>
									
									<div class="hgroup">
										<h1>Video</h1>
										<span class="subtitle">Stay up to date with us</span>
									</div><!-- .hgroup -->
									
								</div><!-- .article-title -->
								
								<div class="article-body">
									
									<p>
										At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
										and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them. Our process ensures
										that you are treated as an individual with customized goals, assessments and progressions. Our driving force is to make you stronger,
										healthier and to fuel your energy. We believe in our systems and the people we entrust to deliver.
									</p>
									
									<p>
										We appreciate the trsut our clients place in us, day in and day out. We know it's earned. We will continue to listen, support, and advise our clients
										with the same commitement as the day we first met.
									</p>
									
								</div><!-- .article-content -->
								
							</div><!-- .article-flex -->
							
						</div><!-- .sw -->
					</section><!-- .bluegrad -->
					
				</article>
				
				<section class="nopad">
					<div class="section-links">
						<a href="#">News</a>
						<a href="#">Tips</a>
						<a href="#">Photos</a>
						<a href="#" class="selected">Videos</a>
					</div>
				</section>
				
				<section class="nopad">
				
					<div class="filter-area">
						<div class="filter-bar">
							
								<div class="filter-controls">
									<button class="previous">Prev</button>
									<button class="next">Next</button>
								</div><!-- .filter-controls -->
							
								<div class="count">
									<span class="num">15</span> Videos Found
								</div><!-- .count -->
								
						</div><!-- .filter-bar -->
						
						<div class="filter-content">							
							<div class="grid hoverable-grid sm-pad5">
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://vimeo.com/105192180" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #1" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #2" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://vimeo.com/105192180" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #3" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #4" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://vimeo.com/105192180" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #1" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #2" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://vimeo.com/105192180" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #3" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #4" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://vimeo.com/105192180" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #1" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #2" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://vimeo.com/105192180" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #3" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
								<div class="col col-4 sm-col-3 xs-col-2">
									<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #4" data-gallery="video-gallery" class="mpopup item vid lazybg ar" data-ar="50"></a>
								</div><!-- .col -->
							</div><!-- .grid -->
						</div><!-- .filter-content -->
					</div><!-- .filter-area -->

				</section>

				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>