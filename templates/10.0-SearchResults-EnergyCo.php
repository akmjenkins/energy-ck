<?php $bodyclass = 'search'; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">

				<section class="dark-bg bluegrad">
					<div class="sw">
						
						<div class="article-flex">
							<div class="article-title">
								<span class="circle-button big blue fa-search">Search Results</span>
								
								<div class="hgroup">
									<h1>Search Results</h1>
									<span class="subtitle"><strong>9</strong> results found for "Query"</span>
								</div><!-- .hgroup -->
								
							</div><!-- .article-title -->
							
							<div class="article-body">
								
								<p>
									At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
									and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them.
								</p>
								
							</div><!-- .article-content -->
							
						</div><!-- .article-flex -->
						
					</div><!-- .sw -->
				</section><!-- .bluegrad -->
				
				<section class="nopad">
				
					<div class="filter-area">
					
						<div class="section-title section-header">
							<h2 class="title">News Results</h2>
						</div><!-- .section-title -->
					
						<div class="filter-bar">
							
								<div class="filter-controls">
									<button class="previous">Prev</button>
									<button class="next">Next</button>
								</div><!-- .filter-controls -->
							
								<div class="count">
									<span class="num">5</span> Articles Found
								</div><!-- .count -->
								
						</div><!-- .filter-bar -->
						
						<div class="filter-content">							
							<div class="grid nopad eqh hoverable-grid dark-blocks">
							
								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->
								
							</div><!-- .grid -->
						</div><!-- .filter-content -->
					</div><!-- .filter-area -->

				</section><!-- .nopad -->
				
				<section class="nopad">
				
					<div class="filter-area">
					
						<div class="section-title section-header">
							<h2 class="title">Tips</h2>
						</div><!-- .section-title -->
					
						<div class="filter-bar">
							
								<div class="filter-controls">
									<button class="previous">Prev</button>
									<button class="next">Next</button>
								</div><!-- .filter-controls -->
							
								<div class="count">
									<span class="num">2</span> Tips Found
								</div><!-- .count -->
								
						</div><!-- .filter-bar -->
						
						<div class="filter-content">							
						
							<div class="grid nopad eqh article-blocks">
								<div class="col col-2 xs-col-1">
									<div class="item">
										<div class="pad-40 sm-pad-20 center">
											
											<h3>Curabitur malesuada laoreet elit non</h3>
											
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
												congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
												Donec efficitur iaculis elementum. Vestibulum iaculis fermentum pharetra. Proin varius lorem massa, a pharetra nisi dapibus in. 
												Pellentesque non hendrerit diam. Cras venenatis nisl sed leo convallis euismod a at sem. Sed eget dapibus sapien, lacinia semper 
												leo. Donec sit amet convallis odio. Ut ut dolor ac elit commodo feugiat. Aliquam vitae tincidunt elit. Integer porta felis ut varius lobortis.
											</p>
											
											
										</div><!-- .pad-40 -->
									</div><!-- .item -->
								</div><!-- .col-2 -->
								<div class="col col-2 xs-col-1">
									<div class="item">
										<div class="pad-40 sm-pad-20 center">
											
											<h3>Curabitur malesuada laoreet elit non</h3>
											
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
												congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
												Donec efficitur iaculis elementum.
											</p>
											
											
										</div><!-- .pad-40 -->
									</div><!-- .item -->
								</div><!-- .col-2 -->							
							</div><!-- .grid -->
						
						</div><!-- .filter-content -->
					</div><!-- .filter-area -->

				</section><!-- .nopad -->

				<section class="nopad">
				
					<div class="filter-area">
					
						<div class="section-title section-header">
							<h2 class="title">Page Results</h2>
						</div><!-- .section-title -->
					
						<div class="filter-bar">
							
								<div class="filter-controls">
									<button class="previous">Prev</button>
									<button class="next">Next</button>
								</div><!-- .filter-controls -->
							
								<div class="count">
									<span class="num">2</span> Pages Found
								</div><!-- .count -->
								
						</div><!-- .filter-bar -->
						
						<div class="filter-content">							
							<div class="grid nopad eqh hoverable-grid dark-blocks fill">
							
								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a Page Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a Page Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->
								
							</div><!-- .grid -->
						</div><!-- .filter-content -->
					</div><!-- .filter-area -->

				</section><!-- .nopad -->
				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>