<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">

			
				<article>
				
					<section class="dark-bg bluegrad">
						<div class="sw">
							
							<div class="article-flex">
								<div class="article-title">
									<span class="circle-button big blue energy-f en-f-strength-o">Strength</span>
									
									<div class="hgroup">
										<h1>Strength</h1>
										<span class="subtitle">Programs tailored to you.</span>
									</div><!-- .hgroup -->
									
								</div><!-- .article-title -->
								
								<div class="article-body">
									
									<p>
										At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
										and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them. Our process ensures
										that you are treated as an individual with customized goals, assessments and progressions. Our driving force is to make you stronger,
										healthier and to fuel your energy. We believe in our systems and the people we entrust to deliver.
									</p>
									
									<p>
										We appreciate the trsut our clients place in us, day in and day out. We know it's earned. We will continue to listen, support, and advise our clients
										with the same commitement as the day we first met.
									</p>
									
								</div><!-- .article-content -->
								
							</div><!-- .article-flex -->
							
						</div><!-- .sw -->
					</section><!-- .nopad -->
					
					<section class="lazybg white-grad right-half half-section" data-src="../assets/images/temp/temp-bg.jpg">
						<div class="sw">
							
							<div class="article-body">
								<h2 class="section-title">Results Matter</h2>
								
								<p>
									We focus on enhancing individual performance with a safe and injury-free mindset built upon our fundamentals of energy: Our training sessions
									start with a foam roll to ensure bad movement patterns are reduced and potentially eliminated. We then move to progression based strength training,
									then we finish with more stretching and cool down to ensure your body starts the recovery progress properly. We do all this according to your situation,
									current strength and fitness and goals. Our goal is to provide the best training experience possible while making you move better and more efficiently. 
								</p>
								
							</div><!-- .article-body -->
							
						</div><!-- .sw -->
					</section>
					
					<section class="nopad">
						<div class="grid nopad eqh article-blocks">
						
							<div class="col col-2 xs-col-1">
							
								<div class="item">
									<div class="item-content pad-40 sm-pad-20">
									
										<span class="circle-button blue fa-users">Membership Features</span>
										
										<h4>Membership Features</h4>
										
										<ul>
											<li>Towel Service</li>
											<li>Drop back whenever you want</li>
											<li>Private Facility with maximum membership</li>
										</ul>
									
									</div><!-- .pad-40 -->
								</div><!-- .item -->
								
							</div><!-- .col -->
							
							<div class="col col-2 xs-col-1">
							
								<div class="item">
									<div class="item-content pad-40 sm-pad-20">
									
										<span class="circle-button blue fa-heart">Health &amp; Wellness</span>
										
										<h4>Health &amp; Wellness</h4>
										
										<ul>
											<li>
												Goal setting and wellness plan to support individual goals.
											</li>
											<li>
												Nutritional plan customized for home, office, and travel requirements with scheduled meeting.
											</li>
											<li>
												Online access to all programs and coaching.
											</li>
										</ul>
									
									</div><!-- .pad-40 -->
								</div><!-- .item -->
								
							</div><!-- .col -->
							
							<div class="col col-2 xs-col-1">
							
								<div class="item">
									<div class="item-content pad-40 sm-pad-20">
									
										<span class="circle-button blue energy-f en-f-strength">Fitness</span>
										
										<h4>Fitness</h4>
										
										<ul>
											<li>
												Fitness testing and assessments for cardiovascular and strength baselines
												and performance improvement.
											</li>
											<li>
												An accountability framework is built into the program - the 1st workout of every month is
												testing against your assesments. 
											</li>
											<li>
												Body composition analysis. (BMI, girth, Weight, Body Fat %) Personalized exercise plans for
												home, gym, hotel rooms (cardio and resistance)
											</li>
											<li>
												Movement prep plan and stretching plans
											</li>
											<li>
												Injury management and recovery plan (as needed). Stretching plan for pre- and post-training.
											</li>
											<li></li>
										</ul>
									
									</div><!-- .pad-40 -->
								</div><!-- .item -->
								
							</div><!-- .col -->
							
							<div class="col col-2 xs-col-1">
							
								<div class="item">
									<div class="item-content pad-40 sm-pad-20">
									
										<span class="circle-button blue fa-rss">Member Communications</span>
										
										<h4>Support Platforms for Member Communications</h4>
										
										<ul>
											<li>
												Personal Training Sessions
											</li>
											<li>
												Instant messaging.
											</li>
											<li>
												Apps for phone and tablets (so you always have access).
											</li>
											<li>
												Access to personalized webpage containing your customized plans.
											</li>
											<li>
												Email
											</li>
											<li>
												Phone
											</li>
										</ul>
									
									</div><!-- .pad-40 -->
								</div><!-- .item -->
								
							</div><!-- .col -->
							
						</div><!-- .grid.nopad.eqh -->
					</section>
					
				</article>
				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>