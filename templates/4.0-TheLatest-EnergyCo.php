<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">

			
				<article>
				
					<section class="dark-bg bluegrad">
						<div class="sw">
							
							<div class="article-flex">
								<div class="article-title">
									<span class="circle-button big blue fa-calendar">The Latest</span>
									
									<div class="hgroup">
										<h1>The Latest</h1>
										<span class="subtitle">Stay up to date with us.</span>
									</div><!-- .hgroup -->
									
								</div><!-- .article-title -->
								
								<div class="article-body">
									
									<p>
										At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
										and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them. Our process ensures
										that you are treated as an individual with customized goals, assessments and progressions. Our driving force is to make you stronger,
										healthier and to fuel your energy. We believe in our systems and the people we entrust to deliver.
									</p>
									
									<p>
										We appreciate the trsut our clients place in us, day in and day out. We know it's earned. We will continue to listen, support, and advise our clients
										with the same commitement as the day we first met.
									</p>
									
								</div><!-- .article-content -->
								
							</div><!-- .article-flex -->
							
						</div><!-- .sw -->
					</section><!-- .bluegrad -->
					
				</article>
				
				<section class="nopad">
					<div class="section-links">
						<a href="#">News</a>
						<a href="#">Tips</a>
						<a href="#">Photos</a>
						<a href="#">Videos</a>
					</div>
				</section>
				
				<section class="nopad">
				
					<div class="section-title section-header">
						<h2 class="title">Latest News</h2>
						<span class="subtitle">Stay up to date with The Energy Co.</span>
					</div><!-- .section-title -->
					
					<div class="grid nopad eqh hoverable-grid dark-blocks">
					
						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
								<div class="grid-content-block center">
									<div class="section-title section-header">
										<h3 class="title">This is a News Title</h3>
										<span class="subtitle">This is a news subtitle</span>
									</div><!-- .section-title -->
									<time datetime="2014-12-10">December 10, 2014</time>
									<span class="button">Read More</span>
								</div><!-- .grid-content-block -->
							</a>
						</div><!-- .col -->

						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
								<div class="grid-content-block center">
									<div class="section-title section-header">
										<h3 class="title">This is a News Title</h3>
										<span class="subtitle">This is a news subtitle</span>
									</div><!-- .section-title -->
									<time datetime="2014-12-10">December 10, 2014</time>
									<span class="button">Read More</span>
								</div><!-- .grid-content-block -->
							</a>
						</div><!-- .col -->

						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
								<div class="grid-content-block center">
									<div class="section-title section-header">
										<h3 class="title">This is a News Title</h3>
										<span class="subtitle">This is a news subtitle</span>
									</div><!-- .section-title -->
									<time datetime="2014-12-10">December 10, 2014</time>
									<span class="button">Read More</span>
								</div><!-- .grid-content-block -->
							</a>
						</div><!-- .col -->

						<div class="col col-4 sm-col-2 xs-col-1">
							<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
								<div class="grid-content-block center">
									<div class="section-title section-header">
										<h3 class="title">This is a News Title</h3>
										<span class="subtitle">This is a news subtitle</span>
									</div><!-- .section-title -->
									<time datetime="2014-12-10">December 10, 2014</time>
									<span class="button">Read More</span>
								</div><!-- .grid-content-block -->
							</a>
						</div><!-- .col -->
						
					</div><!-- .grid -->
					
					<div class="view-all">
						<a href="#" class="button big">View All News</a>
					</div><!-- .center -->

				</section><!-- .nopad -->
				
				<section class="nopad lightbg">
					<div class="section-title section-header">
						<h2 class="title">Latest Tips</h2>
						<span class="subtitle">We are always here to help.</span>
					</div><!-- .section-title -->
				</section><!-- .nopad -->
				
				<section class="nopad">
					<div class="grid nopad eqh article-blocks">
					
						<div class="col col-2 xs-col-1">
							<div class="item">
								<div class="pad-40 sm-pad-20 center">
									
									<h3>Curabitur malesuada laoreet elit non</h3>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
										congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
										Donec efficitur iaculis elementum. Vestibulum iaculis fermentum pharetra. Proin varius lorem massa, a pharetra nisi dapibus in. 
										Pellentesque non hendrerit diam. Cras venenatis nisl sed leo convallis euismod a at sem. Sed eget dapibus sapien, lacinia semper 
										leo. Donec sit amet convallis odio. Ut ut dolor ac elit commodo feugiat. Aliquam vitae tincidunt elit. Integer porta felis ut varius lobortis.
									</p>
									
									
								</div><!-- .pad-40 -->
							</div><!-- .item -->
						</div><!-- .col-2 -->
						
						<div class="col col-2 xs-col-1">
							<div class="item">
								<div class="pad-40 sm-pad-20 center">
									
									<h3>Curabitur malesuada laoreet elit non</h3>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
										congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
										Donec efficitur iaculis elementum.
									</p>
									
									
								</div><!-- .pad-40 -->
							</div><!-- .item -->
						</div><!-- .col-2 -->
						
						<div class="col col-2 xs-col-1">
							<div class="item">
								<div class="pad-40 sm-pad-20 center">
									
									<h3>Curabitur malesuada laoreet elit non</h3>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
										congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
										Donec efficitur iaculis elementum.
									</p>
									
									
								</div><!-- .pad-40 -->
							</div><!-- .item -->
						</div><!-- .col-2 -->
						
						<div class="col col-2 xs-col-1">
							<div class="item">
								<div class="pad-40 sm-pad-20 center">
									
									<h3>Curabitur malesuada laoreet elit non</h3>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
										congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
										Donec efficitur iaculis elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et 
										tristique. Cras vel sapien scelerisque, congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. 
										Ut maximus eget felis a laoreet. Donec efficitur iaculis elementum.
									</p>
									
									
								</div><!-- .pad-40 -->
							</div><!-- .item -->
						</div><!-- .col-2 -->
						
					</div><!-- .grid -->
					
					<div class="view-all">
						<a href="#" class="button big">View All Tips</a>
					</div><!-- .center -->
					
				</section><!-- .nopad -->
				
				<section class="nopad dark-bg bluebg">
				
					<div class="grid eqh latest-galleries">
					
						<div class="col col-2 sm-col-1">
						
							<div class="item pad-20">
							
								<div class="section-title section-header">
									<h2 class="title">Latest Photos</h2>
									<span class="subtitle">A glimpse inside The Energy Co.</span>
								</div><!-- .section-title -->
								
								<div class="grid hoverable-grid fill">
									<div class="col col-3 xs-col-2">
										<a href="../assets/images/temp/temp-video.jpg" data-src="../assets/images/temp/temp-video.jpg"   class="mpopup item lazybg"></a>
									</div><!-- .col -->
									<div class="col col-3 xs-col-2">
										<a href="../assets/images/temp/temp-video.jpg" data-src="../assets/images/temp/temp-video.jpg"  class="mpopup item lazybg"></a>
									</div><!-- .col -->
									<div class="col col-3 xs-col-2">
										<a href="../assets/images/temp/temp-video.jpg" data-src="../assets/images/temp/temp-video.jpg"  class="mpopup item lazybg"></a>
									</div><!-- .col -->
									<div class="col col-3 xs-col-2">
										<a href="../assets/images/temp/temp-video.jpg" data-src="../assets/images/temp/temp-video.jpg"  class="mpopup item lazybg"></a>
									</div><!-- .col -->
									<div class="col col-3 xs-col-2">
										<a href="../assets/images/temp/temp-video.jpg" data-src="../assets/images/temp/temp-video.jpg"   class="mpopup item lazybg"></a>
									</div><!-- .col -->
									<div class="col col-3 xs-col-2">
										<a href="../assets/images/temp/temp-video.jpg" data-src="../assets/images/temp/temp-video.jpg"  class="mpopup item lazybg"></a>
									</div><!-- .col -->
								</div><!-- .grid -->
								
								<div class="view-all">
									<a href="#" class="button big white">View Our Gallery</a>
								</div><!-- .center -->
							
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col col-2 sm-col-1">
						
							<div class="item pad-20">
							
								<div class="section-title section-header">
									<h2 class="title">Latest Videos</h2>
									<span class="subtitle">A glimpse inside The Energy Co.</span>
								</div><!-- .section-title -->
								
								<div class="grid hoverable-grid fill">
									<div class="col col-2 xs-col-1">
										<a href="https://vimeo.com/105192180" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #1" data-gallery="video-gallery" class="mpopup item vid lazybg"></a>
									</div><!-- .col -->
									<div class="col col-2 xs-col-1">
										<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #2" data-gallery="video-gallery" class="mpopup item vid lazybg"></a>
									</div><!-- .col -->
									<div class="col col-2 xs-col-1">
										<a href="https://vimeo.com/105192180" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #3" data-gallery="video-gallery" class="mpopup item vid lazybg"></a>
									</div><!-- .col -->
									<div class="col col-2 xs-col-1">
										<a href="https://www.youtube.com/watch?v=cfR7qxtgCgY" data-src="../assets/images/temp/temp-video.jpg" data-title="Video #4" data-gallery="video-gallery" class="mpopup item vid lazybg"></a>
									</div><!-- .col -->
								</div><!-- .grid -->
								
								<div class="view-all">
									<a href="#" class="button big white">View More Videos</a>
								</div><!-- .center -->
							
							</div><!-- .item -->
						</div><!-- .col -->

						
					</div><!-- .grid -->
					
				</section><!-- .nopad -->

				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>