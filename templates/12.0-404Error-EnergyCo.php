<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">
				
				<section class="dark-bg bluegrad">
					<div class="sw">
						
						<div class="article-flex">
							<div class="article-title">
								<span class="circle-button big blue fa-exclamation-circle">404</span>
								
								<div class="hgroup">
									<h1>404</h1>
									<span class="subtitle">Page Not Found</span>
								</div><!-- .hgroup -->
								
							</div><!-- .article-title -->
							
							<div class="article-body">
								
								<p>
									We're sorry. The page you are looking for cannot be found. But don't worry! You can use the menu button above to see if 
									what you are looking for is in there, or you can use the search function to search our website.
								</p>
								
								<p>
									If you would simply like to return to the page you were previously on, click the button below.
								</p>
								
							</div><!-- .article-body -->
							
						</div><!-- .article-flex -->
						
						<br />
						
						<div class="center">
							<a href="#" class="button big white">Go Back</a>
						</div>
						
					</div><!-- .sw -->
				</section>
				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>