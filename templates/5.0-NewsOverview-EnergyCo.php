<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">

			
				<article>
				
					<section class="dark-bg bluegrad">
						<div class="sw">
							
							<div class="article-flex">
								<div class="article-title">
									<span class="circle-button big blue fa-newspaper-o">News</span>
									
									<div class="hgroup">
										<h1>News</h1>
										<span class="subtitle">Get a glimpse inside The Energy Co.</span>
									</div><!-- .hgroup -->
									
								</div><!-- .article-title -->
								
								<div class="article-body">
									
									<p>
										At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
										and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them. Our process ensures
										that you are treated as an individual with customized goals, assessments and progressions. Our driving force is to make you stronger,
										healthier and to fuel your energy. We believe in our systems and the people we entrust to deliver.
									</p>
									
									<p>
										We appreciate the trsut our clients place in us, day in and day out. We know it's earned. We will continue to listen, support, and advise our clients
										with the same commitement as the day we first met.
									</p>
									
								</div><!-- .article-content -->
								
							</div><!-- .article-flex -->
							
						</div><!-- .sw -->
					</section><!-- .bluegrad -->
					
				</article>
				
				<section class="nopad">
					<div class="section-links">
						<a href="#" class="selected">News</a>
						<a href="#">Tips</a>
						<a href="#">Photos</a>
						<a href="#">Videos</a>
					</div>
				</section>
				
				<section class="nopad">
				
					<div class="filter-area">
						<div class="filter-bar">
							
								<div class="filter-controls">
									<button class="previous">Prev</button>
									<button class="next">Next</button>
								</div><!-- .filter-controls -->
							
								<div class="count">
									<span class="num">30</span> Articles Found
								</div><!-- .count -->
								
						</div><!-- .filter-bar -->
						
						<div class="filter-content">							
							<div class="grid nopad eqh hoverable-grid dark-blocks">
							
								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

							
								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->

								<div class="col col-4 sm-col-2 xs-col-1">
									<a href="#" class="item dark-bg lazybg" data-src="../assets/images/temp/np-2.jpg">
										<div class="grid-content-block center">
											<div class="section-title section-header">
												<h3 class="title">This is a News Title</h3>
												<span class="subtitle">This is a news subtitle</span>
											</div><!-- .section-title -->
											<time datetime="2014-12-10">December 10, 2014</time>
											<span class="button">Read More</span>
										</div><!-- .grid-content-block -->
									</a>
								</div><!-- .col -->
								
							</div><!-- .grid -->
						</div><!-- .filter-content -->
					</div><!-- .filter-area -->

				</section>

				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>