<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-home-hero.php'); ?>

			<div class="body">

				<section class="nopad">
					<div class="lightgrad">
						<div class="section-header">
							<span class="section-header-title">Find Your Focus</span>
							<p>
								The Energy Company offers a complete health and fitness experience for individuals
								as well as corporate organizations and remote environments.
							</p>
						</div><!-- .section-header -->
					</div><!-- .lightgrad -->
				
				
					<div class="grid nopad eqh fill ov-blocks">
						<div class="col col-4 sm-col-2 xs-col-1">
							<a class="item lazybg" href="#" data-src="../assets/images/temp/ov-1.jpg">
							
								<div class="item-content pad-40 sm-pad-20">
								
									<span class="circle-button energy-f blue big en-f-strength-o">Strength</span>
									<span class="ov-title">Strength</span>
									<span class="button">Read More</span>
								
								</div><!-- .item-content -->
							
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a class="item lazybg" href="#" data-src="../assets/images/temp/ov-2.jpg">
							
								<div class="item-content pad-40 sm-pad-20">
								
									<span class="circle-button energy-f blue big en-f-corporate">Corporate</span>
									<span class="ov-title">Corporate</span>
									<span class="button">Read More</span>
								
								</div><!-- .item-content -->
							
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a class="item lazybg" href="#" data-src="../assets/images/temp/ov-3.jpg">
							
								<div class="item-content pad-40 sm-pad-20">
								
									<span class="circle-button energy-f blue big en-f-safety">Safety</span>
									<span class="ov-title">Safety</span>
									<span class="button">Read More</span>
								
								</div><!-- .item-content -->
							
							</a><!-- .item -->
						</div><!-- .col -->
						<div class="col col-4 sm-col-2 xs-col-1">
							<a class="item lazybg" href="#" data-src="../assets/images/temp/ov-4.jpg">
							
								<div class="item-content pad-40 sm-pad-20">
								
									<span class="circle-button energy-f blue big en-f-weights">Atheletics Club</span>
									<span class="ov-title">Atheletics Club</span>
									<span class="button">Read More</span>
								
								</div><!-- .item-content -->
							
							</a><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->
				
				</section><!-- .nopad -->
				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>