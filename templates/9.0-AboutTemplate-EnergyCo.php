<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/temp-9.jpg" alt="employees"/>
			</div><!-- .hero -->

			<div class="body">

			
				<article>
				
					<section class="dark-bg bluegrad">
						<div class="sw">
							
							<div class="article-flex">
								<div class="article-title">
									<span class="circle-button big blue fa-info-circle">About</span>
									
									<div class="hgroup">
										<h1>About</h1>
										<span class="subtitle">Get to Know Us</span>
									</div><!-- .hgroup -->
									
								</div><!-- .article-title -->
								
								<div class="article-body">
									
									<p>
										At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
										and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them. Our process ensures
										that you are treated as an individual with customized goals, assessments and progressions. Our driving force is to make you stronger,
										healthier and to fuel your energy. We believe in our systems and the people we entrust to deliver.
									</p>
									
									<p>
										We appreciate the trsut our clients place in us, day in and day out. We know it's earned. We will continue to listen, support, and advise our clients
										with the same commitement as the day we first met.
									</p>
									
								</div><!-- .article-content -->
								
							</div><!-- .article-flex -->
							
						</div><!-- .sw -->
					</section><!-- .nopad -->
					
					<section class="lazybg white-grad right-half half-section" data-src="../assets/images/temp/temp-bg.jpg">
						<div class="sw">
							
							<div class="article-body">
								<h2 class="section-title">Purpose One of Energy Co.</h2>
								
								<p>
									We focus on enhancing individual performance with a safe and injury-free mindset built upon our fundamentals of energy: Our training sessions
									start with a foam roll to ensure bad movement patterns are reduced and potentially eliminated. We then move to progression based strength training,
									then we finish with more stretching and cool down to ensure your body starts the recovery progress properly. We do all this according to your situation,
									current strength and fitness and goals. Our goal is to provide the best training experience possible while making you move better and more efficiently. 
								</p>
								
							</div><!-- .article-body -->
							
						</div><!-- .sw -->
					</section>
					
					<section class="lazybg white-grad left-half half-section" data-src="../assets/images/temp/temp-bg-2.jpg">
						<div class="sw">
							
							<div class="article-body">
								<h2 class="section-title">Purpose Two of Energy Co.</h2>
								
								<p>
									We focus on enhancing individual performance with a safe and injury-free mindset built upon our fundamentals of energy: Our training sessions
									start with a foam roll to ensure bad movement patterns are reduced and potentially eliminated. We then move to progression based strength training,
									then we finish with more stretching and cool down to ensure your body starts the recovery progress properly. We do all this according to your situation,
									current strength and fitness and goals. Our goal is to provide the best training experience possible while making you move better and more efficiently. 
								</p>
								
							</div><!-- .article-body -->
							
						</div><!-- .sw -->
					</section>
					
					<section class="lazybg white-grad right-half half-section" data-src="../assets/images/temp/temp-bg-3.jpg">
						<div class="sw">
							
							<div class="article-body">
								<h2 class="section-title">Purpose Three of Energy Co.</h2>
								
								<p>
									We focus on enhancing individual performance with a safe and injury-free mindset built upon our fundamentals of energy: Our training sessions
									start with a foam roll to ensure bad movement patterns are reduced and potentially eliminated. We then move to progression based strength training,
									then we finish with more stretching and cool down to ensure your body starts the recovery progress properly. We do all this according to your situation,
									current strength and fitness and goals. Our goal is to provide the best training experience possible while making you move better and more efficiently. 
								</p>
								
							</div><!-- .article-body -->
							
						</div><!-- .sw -->
					</section>
					
					<section class="nopad">							
						<div class="lightgrad">
						
							<div class="section-title section-header">
								<h2 class="title">Our Team</h2>
								<span class="subtitle">Get To Know Us</span>
							</div><!-- .section-title -->
						</div><!-- .light-grad -->
						
						<div class="grid eqh employees nopad">
							<div class="col col-2 sm-col-1">
								<div class="item">
									<div class="employee-panel">
									
										<div class="lazybg">
											<img src="../assets/images/temp/mike.jpg" alt="Mike">
										</div><!-- .lazybg -->
										
										<div class="article-body pad-20 sm-pad-10">
											<h3>Mike O'Neill</h3>
											
											<p>
												Maecenas ex nulla, aliquam ut tempor vitae, accumsan at risus. Pellentesque gravida non ex pellentesque sollicitudin. 
												Nam eu massa sit amet orci laoreet iaculis ut non tortor. Aenean placerat, velit sed tristique mollis, mi ligula dapibus tortor, 
												gravida pulvinar libero nisi ac ante. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque scelerisque 
												volutpat mi, id porta enim lobortis et. Nullam efficitur massa non gravida maximus. Sed congue quam vel turpis vehicula lobortis. Phasellus eget tortor ante.
											</p>
											
										</div><!-- .article-body -->
										
									</div><!-- .employee-panel -->
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2 sm-col-1">
								<div class="item">
									<div class="employee-panel">
									
										<div class="lazybg">
											<img src="../assets/images/temp/mark.jpg" alt="Mike">
										</div><!-- .lazybg -->
										
										<div class="article-body pad-20 sm-pad-10">
											<h3>Employee Name</h3>
											
											<p>
												Maecenas ex nulla, aliquam ut tempor vitae, accumsan at risus. Pellentesque gravida non ex pellentesque sollicitudin. 
												Nam eu massa sit amet orci laoreet iaculis ut non tortor. Aenean placerat, velit sed tristique mollis, mi ligula dapibus tortor, 
												gravida pulvinar.
											</p>
											
										</div><!-- .article-body -->
										
									</div><!-- .employee-panel -->
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2 sm-col-1">
								<div class="item">
									<div class="employee-panel">
									
										<div class="lazybg">
											<img src="../assets/images/temp/emp1.jpg" alt="Mike">
										</div><!-- .lazybg -->
										
										<div class="article-body pad-20 sm-pad-10">
											<h3>Employee Name</h3>
											
											<p>
												Maecenas ex nulla, aliquam ut tempor vitae, accumsan at risus. Pellentesque gravida non ex pellentesque sollicitudin. 
												Nam eu massa sit amet orci laoreet iaculis ut non tortor. Aenean placerat, velit sed tristique mollis, mi ligula dapibus tortor, 
												gravida pulvinar libero nisi ac ante. Interdum et malesuada fames ac ante ipsum primis in faucibus.
											</p>
											
										</div><!-- .article-body -->
										
									</div><!-- .employee-panel -->
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-2 sm-col-1">
								<div class="item">
									<div class="employee-panel">
									
										<div class="lazybg">
											<img src="../assets/images/temp/emp2.jpg" alt="Mike">
										</div><!-- .lazybg -->
										
										<div class="article-body pad-20 sm-pad-10">
											<h3>Employee Name</h3>
											
											<p>
												Maecenas ex nulla, aliquam ut tempor vitae, accumsan at risus. Pellentesque gravida non ex pellentesque sollicitudin. 
												Nam eu massa sit amet orci laoreet iaculis ut non tortor.
											</p>
											
										</div><!-- .article-body -->
										
									</div><!-- .employee-panel -->
								</div><!-- .item -->
							</div><!-- .col -->



						</div>
						
					</section>
					
				</article>
				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>