<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">

			
				<article>
				
					<section class="dark-bg bluegrad">
						<div class="sw">
							
							<div class="article-flex">
								<div class="article-title">
									<span class="circle-button big blue fa-newspaper-o">This is a Single News Post</span>
									
									<div class="hgroup">
										<h1>This is a Single News Post</h1>
										<span class="subtitle">This is a single news post subtitle</span>
									</div><!-- .hgroup -->
									
								</div><!-- .article-title -->
								
								<div class="article-body">
									
									<p>
										At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
										and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them. Our process ensures
										that you are treated as an individual with customized goals, assessments and progressions.
									</p>
									
								</div><!-- .article-content -->
								
							</div><!-- .article-flex -->
							
						</div><!-- .sw -->
					</section><!-- .bluegrad -->
				
					<section class="nopad">
						<div class="section-links">
							<a href="#">News</a>
							<a href="#" class="selected">This is a Single News Post</a>
						</div><!-- .section-links -->
					</section><!-- .nopad -->
					
					<div class="main-body">
					
						<div class="content">
							
							<div class="article-body">
							
								<div class="article-head">
									<time class="meta t-fa fa-calendar" pubdate>December 10, 2014</time>
									
									<span class="meta author">
										
										<div class="lazybg authimage" data-src="../assets/images/temp/auth.jpg">
										</div><!-- .authimage -->
									
										By Mike O'Neil
									</span>
									
									<div class="social">
										Share:
										<a href="#" title="Share {{article_title}} on Facebook" class="circle-button sm blue inverse fb" rel="external">Share {{article_title}} on Facebook</a>
										<a href="#" title="Share {{article_title}} on Twitter" class="circle-button sm blue inverse tw" rel="external">Share {{article_title}} on Twitter</a>
										<a href="#" title="Share {{article_title}} on LinkedIn" class="circle-button sm blue inverse in" rel="external">Share {{article_title}} on LinkedIn</a>
									</div><!-- .social -->
									
								</div><!-- .article-head -->
								
								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam interdum odio in justo malesuada, nec lacinia purus tincidunt. Aliquam auctor dignissim ex sed ultrices. 
									In in auctor odio. Nam tincidunt sit amet justo sit amet congue. Aliquam felis felis, pellentesque vitae ligula in, facilisis pharetra erat. Donec ornare turpis a venenatis tempus. 
									Sed dignissim elit nibh, sed placerat erat fringilla ultricies. Pellentesque semper sit amet sem non semper. Vivamus egestas ut dui id commodo. Duis gravida id diam id 
									accumsan. Ut gravida lorem sapien, eget ultricies tellus ullamcorper non. Pellentesque quis ante volutpat, convallis orci ac, eleifend sem. Integer et ultrices arcu. 
									Mauris urna odio, commodo at tempor in, placerat eu ipsum. Integer placerat odio sit amet vehicula fringilla. Donec condimentum, mauris nec ultrices luctus, mauris 
									enim facilisis velit, eu commodo urna ipsum eu turpis.
								</p>

								<p>
									Integer vitae iaculis arcu. Cras iaculis massa quam, vel fringilla arcu porta non. Donec non velit metus. Aliquam ut purus et diam tincidunt posuere ac at orci. Nulla 
									bibendum libero turpis, non sagittis dui interdum eget. Sed pharetra egestas enim, non accumsan justo finibus et. Curabitur libero nibh, finibus eget tortor lobortis, aliquet 
									condimentum enim. Donec in dolor nunc.
								</p>

								<p>
									Nulla eu nunc ipsum. In sagittis, diam nec commodo fermentum, justo orci tristique elit, eu vulputate orci mi in ligula. Nulla at cursus felis. Sed sit amet purus et tellus auctor 
									pretium id ac lectus. Praesent et pretium ligula. Morbi commodo a urna non vehicula. Donec egestas diam eget quam tincidunt aliquam ac ac nulla. Duis nec leo aliquam, 
									finibus nibh non, tempor arcu. Donec vel velit ac neque congue tristique. Pellentesque vitae nulla scelerisque, condimentum risus quis, tincidunt quam. Vivamus quis ipsum 
									diam. Morbi a tortor eget ex tristique porttitor. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis ut pellentesque quam.
								</p>

								<p>
									Proin rutrum viverra nisl, eget hendrerit neque egestas in. Donec et molestie felis, quis blandit dui. Aenean ut massa magna. Cras ex odio, maximus nec tellus dictum, 
									laoreet vestibulum felis. Cras pharetra neque ac viverra porttitor. Etiam vulputate nunc non nulla dignissim mollis. Suspendisse justo est, eleifend et varius ut, venenatis 
									at elit. Vestibulum et metus massa. Sed et eleifend felis, ac molestie mi. Vestibulum id est ut dolor blandit maximus id eget enim. Suspendisse ullamcorper mauris vel 
									nisl consectetur, at ornare tortor ullamcorper. Sed lorem erat, mollis a lorem sit amet, blandit rhoncus neque. Donec vel lorem a dui gravida porta. Vestibulum nec ante nisi.
								</p>
								
								<h2>Header 2 - H2</h2>
								<h3>Header 3 - H3</h3>
								<h4>Header 4 - H4</h4>
								<h5>Header 5 - H5</h5>
								<h6>Header 6 - H6</h6>
								
								<p>
									<a href="#link" class="link">Inline Link</a> <br />
									<a href="#visited" class="visited">Inline Visited Link</a> <br />
									<a href="#hover" class="hover">Hover Link</a> <br />
								</p>
								
								<br />
								<br />
								
								<a href="#" class="button">Button</a> <br /><br />
								<a href="#" class="button white">White Button</a>
								
								<ul>
									<li>Proin rutrum viverra nisl, eget hendrerit neque egestas in.</li>
									<li>Proin rutrum viverra nisl, eget hendrerit neque egestas in.</li>
									<li>Proin rutrum viverra nisl, eget hendrerit neque egestas in.</li>
								</ul>
								
								<ol>
									<li>Proin rutrum viverra nisl, eget hendrerit neque egestas in.</li>
									<li>Proin rutrum viverra nisl, eget hendrerit neque egestas in.</li>
									<li>Proin rutrum viverra nisl, eget hendrerit neque egestas in.</li>
								</ol>
							
							</div><!-- .article-body -->
						</div><!-- .content -->
						
						<aside class="sidebar">
						
							<div class="archives-mod">
								<h3 class="title">News Archives</h3>
								
								<div class="acc">
									<div class="acc-item">
										<div class="acc-item-handle">
											<span class="count">13</span> 2014
										</div><!-- .acc-item-handle -->
										<div class="acc-item-content">
											<ul>
												<li class="selected"><a href="#"><span class="count">8</span> October</a></li>
												<li><a href="#"><span class="count">5</span> September</a></li>
											</ul>
										</div><!-- .acc-item-content -->
									</div><!-- .acc-item -->
									<div class="acc-item">
										<div class="acc-item-handle">
											<span class="count">15</span> 2013
										</div><!-- .acc-item-handle -->
										<div class="acc-item-content">
											<ul>
												<li><a href="#"><span class="count">3</span> October (3)</a></li>
												<li><a href="#"><span class="count">3</span> September (2)</a></li>
												<li><a href="#"><span class="count">3</span> September (2)</a></li>
												<li><a href="#"><span class="count">3</span> September (2)</a></li>
											</ul>
										</div><!-- .acc-item-content -->
									</div><!-- .acc-item -->
									<div class="acc-item">
										<div class="acc-item-handle">
											<span class="count">16</span> 2013
										</div><!-- .acc-item-handle -->
										<div class="acc-item-content">
											<ul>
												<li><a href="#"><span class="count">3</span> October (3)</a></li>
												<li><a href="#"><span class="count">3</span> September (2)</a></li>
												<li><a href="#"><span class="count">3</span> September (2)</a></li>
												<li><a href="#"><span class="count">3</span> September (2)</a></li>
											</ul>
										</div><!-- .acc-item-content -->
									</div><!-- .acc-item -->
								</div><!-- .acc -->
								
							</div><!-- .archives -->

						
						</aside><!-- .sidebar -->
						
					</div><!-- .main-body -->
				
				</article>

				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>