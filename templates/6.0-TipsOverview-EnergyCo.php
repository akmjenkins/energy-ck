<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">

			
				<article>
				
					<section class="dark-bg bluegrad">
						<div class="sw">
							
							<div class="article-flex">
								<div class="article-title">
									<span class="circle-button big blue fa-lightbulb-o">Tips</span>
									
									<div class="hgroup">
										<h1>Tips</h1>
										<span class="subtitle">We Are Always Here to Help</span>
									</div><!-- .hgroup -->
									
								</div><!-- .article-title -->
								
								<div class="article-body">
									
									<p>
										At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
										and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them. Our process ensures
										that you are treated as an individual with customized goals, assessments and progressions. Our driving force is to make you stronger,
										healthier and to fuel your energy. We believe in our systems and the people we entrust to deliver.
									</p>
									
									<p>
										We appreciate the trsut our clients place in us, day in and day out. We know it's earned. We will continue to listen, support, and advise our clients
										with the same commitement as the day we first met.
									</p>
									
								</div><!-- .article-content -->
								
							</div><!-- .article-flex -->
							
						</div><!-- .sw -->
					</section><!-- .bluegrad -->
					
				</article>
				
				<section class="nopad">
					<div class="section-links">
						<a href="#">News</a>
						<a href="#" class="selected">Tips</a>
						<a href="#">Photos</a>
						<a href="#">Videos</a>
					</div>
				</section>
				
				<section class="nopad">
				
					<div class="filter-area">
						<div class="filter-bar">
							
								<div class="filter-controls">
									<button class="previous">Prev</button>
									<button class="next">Next</button>
								</div><!-- .filter-controls -->
							
								<div class="count">
									<span class="num">24</span> Tips
								</div><!-- .count -->
								
						</div><!-- .filter-bar -->
						
						<div class="filter-content">							
						
							<div class="grid nopad eqh article-blocks">
							
								<div class="col col-2 xs-col-1">
									<div class="item">
										<div class="pad-40 sm-pad-20 center">
											
											<h3>Curabitur malesuada laoreet elit non</h3>
											
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
												congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
												Donec efficitur iaculis elementum. Vestibulum iaculis fermentum pharetra. Proin varius lorem massa, a pharetra nisi dapibus in. 
												Pellentesque non hendrerit diam. Cras venenatis nisl sed leo convallis euismod a at sem. Sed eget dapibus sapien, lacinia semper 
												leo. Donec sit amet convallis odio. Ut ut dolor ac elit commodo feugiat. Aliquam vitae tincidunt elit. Integer porta felis ut varius lobortis.
											</p>
											
											
										</div><!-- .pad-40 -->
									</div><!-- .item -->
								</div><!-- .col-2 -->
								
								<div class="col col-2 xs-col-1">
									<div class="item">
										<div class="pad-40 sm-pad-20 center">
											
											<h3>Curabitur malesuada laoreet elit non</h3>
											
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
												congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
												Donec efficitur iaculis elementum.
											</p>
											
											
										</div><!-- .pad-40 -->
									</div><!-- .item -->
								</div><!-- .col-2 -->
								
								<div class="col col-2 xs-col-1">
									<div class="item">
										<div class="pad-40 sm-pad-20 center">
											
											<h3>Curabitur malesuada laoreet elit non</h3>
											
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
												congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
												Donec efficitur iaculis elementum.
											</p>
											
											
										</div><!-- .pad-40 -->
									</div><!-- .item -->
								</div><!-- .col-2 -->
								
								<div class="col col-2 xs-col-1">
									<div class="item">
										<div class="pad-40 sm-pad-20 center">
											
											<h3>Curabitur malesuada laoreet elit non</h3>
											
											<p>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et tristique. Cras vel sapien scelerisque, 
												congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. Ut maximus eget felis a laoreet. 
												Donec efficitur iaculis elementum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam mollis ac quam et 
												tristique. Cras vel sapien scelerisque, congue quam at, fermentum tellus. Praesent laoreet eleifend arcu at maximus. 
												Ut maximus eget felis a laoreet. Donec efficitur iaculis elementum.
											</p>
											
											
										</div><!-- .pad-40 -->
									</div><!-- .item -->
								</div><!-- .col-2 -->
								
							</div><!-- .grid -->
						
						</div><!-- .filter-content -->
					</div><!-- .filter-area -->

				</section>

				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>