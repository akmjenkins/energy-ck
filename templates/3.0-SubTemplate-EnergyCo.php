<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

			<div class="hero lazybg">
				<img src="../assets/images/temp/inner-hero.jpg" alt="energy gym"/>
			</div><!-- .hero -->

			<div class="body">

			
				<article>
				
					<section class="dark-bg bluegrad">
						<div class="sw">
							
							<div class="article-flex">
								<div class="article-title">
									<span class="circle-button big blue fa-envelope-o">Contact</span>
									
									<div class="hgroup">
										<h1>Contact</h1>
										<span class="subtitle">Get in touch with us</span>
									</div><!-- .hgroup -->
									
								</div><!-- .article-title -->
								
								<div class="article-body">
									
									<p>
										At the Energy Company we don't just train you, we support all facets of your personal health and fitness to build your energy 
										and fuel your optimal performance. Our commitment is the relentless pursuits of your goals as you define them. Our process ensures
										that you are treated as an individual with customized goals, assessments and progressions. Our driving force is to make you stronger,
										healthier and to fuel your energy. We believe in our systems and the people we entrust to deliver.
									</p>
									
									<p>
										We appreciate the trsut our clients place in us, day in and day out. We know it's earned. We will continue to listen, support, and advise our clients
										with the same commitement as the day we first met.
									</p>
									
								</div><!-- .article-content -->
								
							</div><!-- .article-flex -->
							
						</div><!-- .sw -->
					</section>
					
					</article>
					
					<section>
						<div class="sw">
							
							<div class="grid eqh">
							
								<div class="col col-2 sm-col-1">
									<div class="item">
										
										<div class="grid">
											<div class="col col-2-5 xs-col-1">
												<div class="item">
												
													<h3>Address</h3>
													<address>
														25 White Rose Drive <br />
														St. John's, NL A1A 0L2
													</address>
													
													<br />
													
													<div class="rows">
														<span class="row">
															<span class="l">Phone</span>
															<span class="r">709 123 4567</span>
														</span>
														<span class="row">
															<span class="l">Fax</span>
															<span class="r">709 123 8910</span>
														</span>
													</div><!-- .rows -->
													
													<br />
													<br />
													
													<h3>Hours of Operation</h3>
													
													<div class="rows">
														<span class="row">
															<span class="l">Mon - Fri</span>
															<span class="r">9AM - 9PM</span>
														</span>
														<span class="row">
															<span class="l">Saturday</span>
															<span class="r">Closed</span>
														</span>
														<span class="row">
															<span class="l">Sunday</span>
															<span class="r">Closed</span>
														</span>
													</div><!-- .rows -->
												
												</div><!-- .item -->
											</div><!-- .col -->
											<div class="col col-3-5 xs-col-1">
												<div class="item">
													<form action="/" method="post" class="body-form">
														<fieldset>
															<input type="text" name="name" placeholder="Full name">
															<input type="email" name="email" placeholder="Email">
															<textarea name="message" autocomplete="disabled" cols="30" rows="10" placeholder="Message"></textarea>
															<button class="button big" type="submit">Submit</button>
														</fieldset>
													</form><!-- .body-form -->
												</div><!-- .item -->
											</div><!-- .col -->
										</div><!-- .grid -->
										
									</div><!-- .item -->
								</div><!-- .col -->
								
								<div class="col col-2 sm-col-1">
									<div class="item gmap">
										<div class="map" data-markers='[{"position":"47.6180996,-52.7215576","title":"The Energy Company","htmlmarker":"energy-htmlmarker"}]' data-center="47.6180996,-52.7215576" data-zoom="17">&nbsp;</div>
									</div><!-- .item.gmap -->
								</div><!-- .col -->
								
								
							</div><!-- .grid.eqh -->
							
						</div><!-- .sw -->
					</section>
					
				</article>
				
			</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>