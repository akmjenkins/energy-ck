//anchor listeners
(function(context) {

	//pub/sub listeners on the document
	$(document)
	
		//link events
		.on('click','a[rel=external]',function (e) { 
			e.preventDefault();
			window.open(this.href); 
		})
		.on('click','a[rel=popup]',function () { 
			var 	el 				= $(this),
					name			=	el.data('name') || (new Date()).getTime(),
					opts				=	el.data('opts'),
					optsString		=	'';
					
			//make all popups that don't explicity say so have scrollbars and are resizeable
			if(!opts) {
				opts = {scrollbars : 'yes',resizeable:'yes'};
			}
			
			if(typeof opts !== 'object') { opts = $.parseJSON(opts); }
		
			opts.resizable = (opts.resizable) ? opts.resizable : 'yes';
			opts.scrollbars = (opts.scrollbars) ? opts.scrollbars : 'yes';
		
			for(i in opts) {
				//don't allow a window to open that is taller than the browser
				if(i === 'height') {
					opts[i] = (window.screen.height < (+opts[i])) ? window.screen.height-50 : opts[i];
				} else if(i === 'width') {
					opts[i] = (window.screen.width < (+opts[i])) ? window.screen.width-50 : opts[i];
				}
				
				optsString	+= i+'='+opts[i]+',';
			}

			window.open(this.href, name,optsString);
			return false; 
		});
		
}(window[ns]));