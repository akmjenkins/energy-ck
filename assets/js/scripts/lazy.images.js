(function(context) {

	var
		$window = $(window),
		$document = $(document),
		d = context.debounce(),
		windowHeight = $window.innerHeight(),
		updateHeight = false,
		methods = {
		
			getImages: function() {
				return $('.lazybg').filter(function() { return $(this).parent().is(':visible') });
			},
		
			updateLazyImages: function() {
				
				if(updateHeight) {
					windowHeight = $window.innerHeight();
				}
				var topOfLazyImage = $window.scrollTop()+windowHeight;

				this.loadImages(methods.getImages().filter(function() {
						var el = $(this);
						return !el.hasClass('loading') && (el.offset().top-topOfLazyImage < 0 || el.hasClass('immediate'));
					})
				);
				
			},
		
			loadImages: function(images) {
			
				$.each(images,function() {
					var 
						img = $(this),
						source = img.data('src') || img.find('img')[0].src;
						
					img.addClass('loading');
					
					$('<img/>')
						.on('load error',function() {
							img
								.css({backgroundImage:'url('+source+')'})
								.addClass('loaded');
						})
						.attr('src',source)
						
				});
				
			}		
		};
		
	//can be triggered when needed (e.g. in the case of AJAX updates to the dom)
	//via $(document).trigger('updateTemplate.lazyimages'); or just $(document).trigger('updateTemplate');
	$window.on('scroll load resize updateTemplate.lazyimages',function(e) { 
		upateHeight = e.type === 'resize';
		d.requestProcess(function() {
			methods.updateLazyImages();
		}); 
	});

}(window[ns]));