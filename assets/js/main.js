var ns = 'JAC-ENERGY';
window[ns] = {};

// jQuery
// @codekit-prepend "../../bower_components/jquery/dist/jquery.min.js";

// magnific popup
// @codekit-prepend "../../bower_components/magnific-popup/dist/jquery.magnific-popup.min.js";

// Used in hero.js
// @codekit-append "scripts/swipe.js"; 
// @codekit-append "scripts/swipe.srf.js"; 

// debounce - used by a bunch of things
// @codekit-append "scripts/debounce.js";

// @codekit-append "scripts/anchors.external.popup.js"; 
// @codekit-append "scripts/standard.accordion.js";
// @codekit-append "scripts/magnific.popup.js";
// @codekit-append "scripts/responsive.video.js";

// @codekit-append "scripts/map/html.marker.js";
// @codekit-append "scripts/gmap.js";

// @codekit-append "scripts/aspect.ratio.js";
// @codekit-append "scripts/lazy.images.js";

// @codekit-append "scripts/nav.js
// @codekit-append "scripts/hero.js

(function($,context) {



	$(document)
		.on('click','.toggle-search',function() {
			$('body').toggleClass('show-search');
			$('.search-form input').focus();
		});

}(jQuery,window[ns]));